const jwt = require('jsonwebtoken');

const auth = (req, res, next) => {

    let token = req.cookies.token

    if (token) {

        jwt.verify(token, process.env.secretKey, (error, user) => {
            if (error) {
                res.json({ message: "Invalid Token" })
            }
            else {
                console.log("valid")
                req.user = user
                next()
            }
        });
    }
    else {
        res.json({
            message: "Access Denied. Unauthorised User!!!"
        })
    }
};

module.exports = auth;