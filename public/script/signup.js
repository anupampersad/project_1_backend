let terms = document.getElementById('terms');

let submitButton = document.getElementById('submit-button');

submitButton.addEventListener('click', function (event) {

    // ______________________________________

    let fname = document.getElementById('fname');

    if (fname.value.length == 0) {
        fname.placeholder = 'Invalid first name';

        fname.classList.add("invalid")

        // fname = false;
    }
    else {
        fname.classList.remove("invalid")
        fname = true;
    }

    // ______________________________________

    let lname = document.getElementById('lname');

    if (lname.value.length == 0) {
        lname.placeholder = 'Invalid Last name';

        lname.classList.add("invalid")

        lname = false;
    }
    else {
        lname.classList.remove("invalid")
        lname = true;
    }

    // ______________________________________

    let email = document.getElementById('email-address');

    let mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

    if (email.value.match(mailformat)) {

        email.classList.remove("invalid")
        email = true;
    }
    else {
        email.classList.add("invalid");
        email = false;
    }

    // ______________________________________

    let age = document.getElementById('age');

    if ((parseInt(age.value) < 4) ||age.value.length == 0) {
        age.classList.add("invalid");
        age = false;
    }
    else {
        age.classList.remove("invalid")
        age = true;
    }

    // ______________________________________

    let terms = document.getElementById('terms');

    if (terms.checked == false){
        terms.classList.add("invalid");
        terms = false;
    }
    else{
        terms.classList.remove("invalid")
    }

    // ______________________________________

    let password = document.getElementById('password');

    let passwordFormat =  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/

    if (password.value.match(passwordFormat)) {

        // console.log(password.value.match(passwordFormat))

        password.classList.remove("invalid")
        password = true;
    }
    else {
        password.classList.add("invalid");
        password = false;
    }

    // __________Final Check_________________

    if (fname == true && lname == true && email == true && age == true && terms.checked == true && password == true){
    }
    else{
        event.preventDefault()
    }
})
