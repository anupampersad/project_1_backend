let terms = document.getElementById('terms');

let submitButton = document.getElementById('submit-button');

submitButton.addEventListener('click', function (event) {


    // ______________________________________

    let email = document.getElementById('email-address');

    let mailformat = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;

    if (email.value.match(mailformat)) {

        email.classList.remove("invalid")
        email = true;
    }
    else {
        email.classList.add("invalid");
        email = false;
    }

    // ______________________________________


    let password = document.getElementById('password');

    let passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/

    if (password.value.match(passwordFormat)) {

        // console.log(password.value.match(passwordFormat))

        password.classList.remove("invalid")
        password = true;
    }
    else {
        password.classList.add("invalid");
        password = false;
    }

    // __________Final Check_________________

    if (email == true && password == true) {
    }
    else {
        event.preventDefault()
    }
})
