let cards = document.getElementsByClassName('card')
let inputHeader = document.getElementById('input-header');
let levels = document.getElementById('levels');
let seconds = document.getElementById('seconds');
let minutes = document.getElementById('minutes');
let inputForm = document.getElementById('input-form');
let gridContainer = document.getElementById('grid-container');
let movesContainer = document.getElementById('moves');
let timeContainer = document.getElementById('time');
let resetButton = document.getElementById('reset-button')
let quitMessage = document.getElementById('quit-message')


const colorsClassArray = ['red', 'red', 'blue', 'blue', 'greenyellow', 'greenyellow', 'brown', 'brown', 'orange', 'orange', 'pink', 'pink', 'purple', 'purple', 'magenta', 'magenta']

// Shuffling Array 
const shuffleArray = (array) => {

    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1))
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array
}

let relation = {
    "red": "chess-queen",
    "brown": "eye-dropper",
    "blue": "grin-squint-tears",
    "greenyellow": "cat",
    "orange": "dog",
    "pink": "dove",
    "purple": "frog",
    "magenta": "spider"
}

// Input the number of cards from user using enter button
let level = document.getElementsByClassName('levels');

function startGame(level) {

    requiredNumberOfCards = 2 ** (parseInt(level.getAttribute('value')) + 1);

    let gridContainer = document.querySelector('.game-grid-container');

    // Adding all div elements in grid container 
    presentColorArray = colorsClassArray.slice(0, requiredNumberOfCards)
    shuffleArray(presentColorArray)

    for (i = 0; i < requiredNumberOfCards; i++) {

        let newDiv = document.createElement('div');
        newDiv.classList.add("initial-color", "icon");
        newDiv.classList.add("card-container");


        let newCard = document.createElement('div');
        newCard.classList.add("card", presentColorArray[i]);
        newCard.setAttribute('color', `${presentColorArray[i]}`)
        newCard.setAttribute("onclick", `openCard(this)`);

        let icon = document.createElement('i');
        icon.classList.add("fas", "fa-question")
        icon.style.display = 'none'
        newCard.appendChild(icon)

        newDiv.appendChild(newCard)
        gridContainer.appendChild(newDiv);
    }

    // Changing color of div on click
    // for (i = 0; i < cards.length; i++) {

    //     cards[i].addEventListener('click', function (event) {

    //         event.target.parentElement.classList.add("change-color");
    //         event.target.firstElementChild.classList.remove('fa-question')

    //         cardColor = event.target.getAttribute('color')

    //         event.target.firstElementChild.style.display = 'block'
    //         event.target.firstElementChild.classList.add(`fa-${relation[cardColor]}`)
    //     })
    // }

    startTimer()
}



let moves = document.getElementById('current-moves')
let currentMoves = 0;

// Main Logic
memory = []
count = 0
async function openCard(card) {

    card.parentElement.classList.add("change-color");
    card.firstElementChild.classList.remove('fa-question')

    cardColor = card.getAttribute('color')

    card.firstElementChild.style.display = 'block'
    card.firstElementChild.classList.add(`fa-${relation[cardColor]}`)

    currentMoves += 1
    moves.innerText = currentMoves

    // Removing click event from element
    card.setAttribute("onclick", `false`);

    memory.push(card)

    if (memory.length == 2) {

        for (i = 0; i < cards.length; i++) {
            cards[i].setAttribute("onclick", `false`);
        }

        card1Color = memory[0].getAttribute('color');
        card2Color = memory[1].getAttribute('color');

        if (card1Color === card2Color) {

            // Disabling hover
            memory[1].parentElement.classList.remove('card-container');
            memory[0].parentElement.classList.remove('card-container');

            // clearing memory
            memory = []
            count += 2

        }
        else {

            setTimeout(() => {

                memory[0].parentElement.classList.remove('change-color');
                memory[1].parentElement.classList.remove('change-color');

                memory[0].firstElementChild.classList.remove(`fa-${relation[card1Color]}`);
                memory[0].firstElementChild.classList.add('fa-question');
                memory[0].firstElementChild.style.display = 'none'

                memory[1].firstElementChild.classList.remove(`fa-${relation[card2Color]}`);
                memory[1].firstElementChild.classList.add('fa-question');
                memory[1].firstElementChild.style.display = 'none'

                // Resetting onclick properties for cards
                // memory[0].setAttribute("onclick", `openCard(this)`);
                // memory[1].setAttribute("onclick", `openCard(this)`);

                memory = []
            }, 150)
        }

        for (i = 0; i < cards.length; i++) {
            cards[i].setAttribute("onclick", `openCard(this)`);
        }

    }

    let numberOfCards = document.getElementsByClassName('card').length

    if (count == numberOfCards) {

        gridContainer.style.display = 'none'
        inputForm.style.display = 'flex';
        levels.style.display = 'none';
        inputHeader.innerHTML = "Congratulations !! <br> You Won !!"

        setTimeout(() => {

            location.reload()
        }, 2000)
    }
}

let currentSeconds = 0;
let currentMinutes = 0;

function startTimer() {

    resetButton.style.display = 'block';
    inputForm.style.display = 'none';
    gridContainer.style.display = 'flex';
    movesContainer.style.display = "block"
    timeContainer.style.display = "block"

    setInterval(function () {

        currentSeconds += 1

        if (currentSeconds == 60) {

            currentSeconds = 0
            currentMinutes += 1

            minutes.innerText = currentMinutes
        }

        seconds.innerText = currentSeconds


    }, 1000);
}

document.getElementById('quit-message')

function message() {
    resetButton.style.display = "none";
    quitMessage.style.display = 'block';
}

function yes() {
    location.reload()
}

function no(){
    resetButton.style.display = "block";
    quitMessage.style.display = 'none';
}