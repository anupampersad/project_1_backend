const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const user = require('../models/user')

const bcrypt = require('bcrypt');
const saltRounds = 10;

const jwt = require('jsonwebtoken')

router.post('/users/login', async (req, res) => {

    ({ email, password } = req.body)

    const userReq = await user.findOne({

        where: {
            email: email
        },
        attributes: {
            exclude: ['createdAt', 'updatedAt']
        }

    });

    if (userReq == null) {
        return res.status(404).json({ message: "No user registered with email id" })
    }

    const match = await bcrypt.compare(password, userReq.password);

    if (match) {

        userReq.password = undefined;
        // This is because we do not want to send password in jsontoken
        // Once we sign using below step, in 'result : userReq.dataValues'  we are passing all the data of the user
        // which is fetched from above using querry except password because that is made undefined
        // Instead of using jwt.sign({ result: userReq.dataValues } ,
        // it will be better to sign with above method i.e. jwt.sign({ ... userReq.dataValues }

        const jsonToken = jwt.sign({ ...userReq.dataValues }, process.env.secretKey, {
            expiresIn: "1h"
        });

        userID = userReq.dataValues.userID;
        res.cookie('token', jsonToken, { maxAge: 3600000 });
        res.json({message:'user logged in successfully'})
        // res.redirect(`/users/${userID}/game`)

    }
    else {
        res.status(400).json({ "message": "User ID or Password are Incorrect" })
    }
})

module.exports = router; 