const express = require('express');
const router = express.Router();
const db = require('../database/connection');

const auth = require('../middlewares/auth');

const user = require('../models/user')

const bcrypt = require('bcrypt');
const saltRounds = 10;

// Add a new user
router.post('/users', async (req, res) => {

    try {
        const password = req.body.password;
        const encryptedPassword = await bcrypt.hash(password, saltRounds);

        const addUser = { ...req.body, password: encryptedPassword };

        await user.create(addUser);

        res.json({ message: "User Added Successfully" });
    }
    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})
// Get a particular user
router.get('/users/:userID/game',  auth ,async (req, res) => {

    try {
        if (req.params.userID == req.user.userID) {

            const users = await user.findByPk(req.params.userID, {
                attributes: {
                    exclude: ['createdAt', 'updatedAt','password','age']
                }
            });

            if (users == null) {
                return res.json({message:"User no longer exists"})
            }

            res.render("index",{users})

        }
        else {
            res.status(401).json({ message: "Unauthorised" })
        }
    }

    catch (error) {
        console.log(error)
        res.status(400).json({ "message": "Invalid Request" })
    }
})

module.exports = router;