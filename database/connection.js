const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.database, process.env.user, process.env.password, {
    host: process.env.host,
    dialect: 'mysql'
});

const connectDB = async () => {
    
    try {
        await sequelize.authenticate();
        console.log("CONNECTION OPEN"); 
    }
    catch (error) {
        console.log("DB CONNECTION FAILED");
    } 
}

sequelize.sync({
    force:false,
    logging:true
})

module.exports = connectDB;

global.sequelize = sequelize;