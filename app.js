require('dotenv').config()
const express = require('express');
const path = require('path');

// Setting up express app
const app = express();
app.use('/', express.static(path.join(__dirname, './public')));

// Setting up ejs templating engine
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));

// Parsing body and json
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Connecting to the database
const connectDB = require('./database/connection');
connectDB()
const cookieParser = require('cookie-parser') 
app.use(cookieParser())

//Requiring Models
const user = require('./models/user')

// Requiring user routes
const userRoutes = require('./routes/user')
const authenticateUser = require('./routes/authenticateUser')

app.use(express.urlencoded({ extended: true }));
app.use(userRoutes);
app.use(authenticateUser);

// Routes
app.get('/', (req, res) => {
    res.render("signup")
})

app.get('/login', (req, res) => {
    res.render("login")
})

// app.get('/users/:userID/game', (req, res) => {
//     res.render("index")
// })

// Listening port
app.listen(8080, () => {
    console.log("Server running on port 8080")
});
